# Based on the Latex SConstruct at
# https://gutefee.massey.ac.nz/moin/HOWTO/LaTeX/LaTeX-SCons-Builder
#
import os
import os.path
import glob

#### Some configurations.

DEFAULT_TARGET = 'Report.pdf'

#### Defining some new builders.
env = Environment()

## Context MkIV Builder. Allow once=1 to just run once without fixing
# all references and tables.
if ARGUMENTS.get('once'):
	once = ' --once'
else:
	once = ''

# Limit to 500MB of virtual memory
contextBuilder = Builder(action='ulimit -v 512000; nice texexec --lua $SOURCE%s' % once,
                         suffix='.pdf',
                         src_suffix='.tex')
env.Append(BUILDERS={'Context': contextBuilder})

# Import tex settings from the user environment
env['ENV']['TEXMFCNF'] = os.environ['TEXMFCNF']
env['ENV']['HOME'] = os.environ['HOME']
# Allow the SSH agent to be used
env['ENV']['SSH_AUTH_SOCK'] = os.environ['SSH_AUTH_SOCK']

#### The actual builds.

## Core2Core document
core2core = env.Context('Core2Core')
Depends(core2core, 'pret-lam.lua')
Depends(core2core, 'pret-trans.lua')

## Report
report = env.Context('Report')
Depends(report, glob.glob('pret-*.lua'))
Depends(report, glob.glob('Chapters/*.tex'))
Depends(report, glob.glob('Utils/*.tex'))
Depends(report, glob.glob('Titlepage.tex'))

AddPostAction(report, 'scp $TARGET ewi:')

Default(DEFAULT_TARGET)
