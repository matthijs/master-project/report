-- filename : type-haskell.lua
-- comment  : Pretty printing of haskell programs. Currently, this is just
---           verbatim printing with the option of applying strikethrough
-- author   : Matthijs Kooijman, Universiteit Twente, NL
-- copyright: Matthijs Kooijman
-- license  : None

local utf = unicode.utf8

local visualizer = buffers.newvisualizer('haskell')

-- Print a string or table of strings with the catcodes set to the default
-- context ones. Use this function to print a string that contains context
-- macro's that should be interpreted. Does not insert any automatic
-- linebreaking.
local function ctxsprint(str) 
    tex.sprint(tex.ctxcatcodes, str)
end

-- Print a string or table of strings almost literal. Each character in the
-- string will be printed with the "character" catcode, causing it to show up
-- literal in the output. Only the space character is printed with the "space"
-- catcode.
local function texwrite(str)
    tex.write(str)
end 

function visualizer.flush_line(str,nested)
    while str ~= '' do
        if utf.match(str, '^ ') then 
            ctxsprint('\\obs ')
            -- Eat the first character
            str = utf.sub(str, 2)
        else
            local text, rest = utf.match(str, "^%-%-(.-)%-%-(.*)")
            if text then
                ctxsprint('\\strikethrough{')
                -- Recursively call ourselves to handle spaces gracefully.
                visualizer.flush_line(text)
                ctxsprint('}')
                -- Eat the processed characters
                str = rest
            elseif utf.match(str, "^%-%-") then
                ctxsprint('{\\italic{--')
                -- Recursively call ourselves to handle spaces gracefully.
                visualizer.flush_line(utf.sub(str, 3))
                ctxsprint('}}')
                -- Done with this line
                str = ''
            else
                -- Write the first character
                texwrite(utf.sub(str, 1, 1))
                -- Eat the first character
                str = utf.sub(str, 2)
            end
        end
    end
end

-- vim: set sw=4 sts=4 expandtab ai:
