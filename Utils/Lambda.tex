%
% Some utilities for formatting (extended) lambda calculus and
% transformations.
%

\startuseMPgraphic{HLine}
path a, b;
% Create a rectangle of the full size
a := origin -- (\MPvar{width}, 0mm);
% Randomize it a bit
b := a randomized (\overlaywidth / 50) ;
% And draw it
drawoptions (withpen pencircle scaled .75mm withcolor black) ;
draw b; 
\stopuseMPgraphic

% Draw a line with the given options. Accepts only the 'width' option. Be
% careful that these are metapost options, so use \the\textwidth instead of
% textwidth, and a metapost expression for arithmethic (e.g. 0.5 *
% \the\textwidth instead of 0.5\textwidth).
\def\HLine[#1]%
  {\setupMPvariables[HLine][#1]%
  \useMPgraphic{HLine}}

\startuseMPgraphic{HDLine}
path a, b;
% Create a rectangle of the full size
a := origin -- (\MPvar{width}, 0mm);
% Randomize it a bit
b := a randomized (\overlaywidth / 100) ;
% And draw it
drawoptions (withpen pencircle scaled .75mm withcolor black dashed evenly) ;
draw b; 
\stopuseMPgraphic

% Draw a line with the given options. Accepts only the 'width' option. Be
% careful that these are metapost options, so use \the\textwidth instead of
% textwidth, and a metapost expression for arithmethic (e.g. 0.5 *
% \the\textwidth instead of 0.5\textwidth).
\def\HDLine[#1]%
  {\setupMPvariables[HDLine][#1]%
  \useMPgraphic{HDLine}}

\startuseMPgraphic{box}
path a, b;
% Create a rectangle of the full size
a := unitsquare xyscaled(\overlaywidth,\overlayheight);
% Randomize it a bit
b := a randomized (min(\overlayheight, \overlaywidth) / 25) ;
% And draw it
drawoptions (withpen pencircle scaled .75mm withcolor black) ;
draw b; 
\stopuseMPgraphic

\defineoverlay[box][\useMPgraphic{box}]

% Define \{start,stop}boxed with a nice metapost box around it.
\defineframedtext[boxed][width=fit,background=box,frame=off]

% Install the lambda calculus pretty-printer, as defined in pret-lam.lua.
\installprettytype [LAM] [LAM]
% Define \startlambda \stoplambda. Suppress page breaks directly before
% such a box (\page[no] only works directly after a paragraph break, so
% insert one).
\definetyping[lambda][option=LAM,style=sans,before={\par\page[no]\startboxed},after={\stopboxed},strip=auto]
% Define \startunboxedlambda \stopunboxedlambda
\definetyping[unboxedlambda][option=LAM,style=sans,strip=auto]

% Define \lam{} (since \lambda is taken)
\definetype[lam][option=LAM,style=sans]

% Install the transformation pretty-printer, as defined in pret-trans.lua.
\installprettytype [TRANS] [TRANS]
% Define \starttrans \stoptrans
\definetyping[trans][option=TRANS,style=normal,before={\par\page[no]\startboxed},after={\stopboxed},strip=auto]

% Install the haskell pretty-printer, as defined in pret-haskell.lua.
\installprettytype [HASKELL] [HASKELL]
% Define \starthaskell \stophaskell
\definetyping[haskell][option=HASKELL,before={\par\page[no]\startboxed},after={\stopboxed},strip=auto]
% Define \hs
\definetype[hs][option=HASKELL,style=mono]

% Define \startvhdl \stopvhdl
\definetyping[vhdl][numbering=line,before={\startboxed},after={\stopboxed}]

% Type the given buffer with the lambda typing style.
% e.g., \typebufferlam{buffname}
\define[1]\typebufferlam{
  % We can't use \startlambda here defined by definetyping[lambda] above when
  % typing buffers, so instead we'll redfine the options here.
  \setuptyping[option=LAM,style=sans,before=,after=,strip=auto]
  \startboxed
  \typebuffer[#1]
  \stopboxed
  % Reset the typing options
  \setuptyping[option=none,style=\tttf,strip=auto]
}

% Type the given buffer with the haskell typing style.
% e.g., \typebufferhs{buffname}
\define[1]\typebufferhs{
  % We can't use \startlambda here defined by definetyping[lambda] above when
  % typing buffers, so instead we'll redfine the options here.
  \setuptyping[option=HASKELL,style=mono,strip=auto]
  \startboxed
  \typebuffer[#1]
  \stopboxed
  % Reset the typing options
  \setuptyping[option=none,style=\tttf,strip=auto]
}

% Type the given buffer with the vhdl typing style.
% e.g., \typebuffervhdl{buffname}
\define[1]\typebuffervhdl{
  % We can't use \startvhdl here defined by definetyping[vhdl] above when
  % typing buffers, so instead we'll redefine the options here.
  \setuptyping[style=mono,strip=auto,numbering=line]
  \startboxed
  \typebuffer[#1]
  \stopboxed
  % Reset the typing options
  \setuptyping[option=none,style=\tttf,strip=auto]
}

% Display a useMPgraphic in a pretty box
\define[1]\boxedgraphic{
  \startboxed
  \useMPgraphic{#1}
  \stopboxed
}

\setupcaption[figure][location=top] % Put captions on top
% Define an "example" float. Don't add box around it, since an example will
% commonly contain two boxed items (Before / after, code / picture).
% Make an example use the same numbering as a figure.
\definefloat[example][examples][figure]
\setupcaption[example][location=top] % Put captions on top
% Define a definition float that shares its numbering and setting with
% examples.
\definefloat[definition][definitions][figure]
\setupcaption[example][location=top] % Put captions on top
% Make sure the labels really say Definition and Example instead of
% Figure. This seems to be a bug in ConTeXt (\redodefinefloat in
% strc-flt.mkiv has \presetlabeltext[#1=\Word{#3}~]% which should
% reference #1 instead of #3).
\setuplabeltext[en][definition=Definition~]
\setuplabeltext[en][example=Example~]

% Margin magic taken from
% http://www.pragma-ade.com/general/manuals/details.pdf By setting negative
% margin distances, we put our float inside the outer margin. I think we set
% both left and right margin distance, because we don't know what will be the
% outer margin (and it will probably only use the distance of the margin it's
% actually aligning against).
% The default= option corresponds to the first option to \placefloat.
% outer puts it in the outer margin, none means no caption, high means
% no spacing at the top (placing it high wrt the text) and low means no
% spacing at the bottom.
\setupfloat[intermezzo][leftmargindistance=-\leftmarginwidth, rightmargindistance=-\rightmarginwidth, default={outer,none,low,high}]

% Let floats float next to titles, instead of inserting whitespace so a
% section head comes after a float.
\setupheads[aligntitle=float]

% A variant of \startquotation that puts a citation directly after the
% quote.
\long\def\startcitedquotation[#1]#2\stopcitedquotation
    {\bgroup \par
     \startnarrower
       \symbol[leftquotation]%
       #2\removeunwantedspaces
       \symbol[rightquotation]%
       \mbox{ }\cite[#1]
     \stopnarrower
     \par \egroup}

% vim: set sw=2 sts=2 expandtab:
