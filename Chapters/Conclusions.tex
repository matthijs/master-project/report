\chapter[chap:conclusions]{Conclusions}
The product of this research is a system called Cλash, which allows hardware
descriptions written in Haskell to be translated to \VHDL and be programmed
into an \small{FPGA}.

In this research, we have seen that a functional language is well suited
for hardware descriptions. Function applications provide elegant notation for
component instantiation and the various choice mechanisms (pattern matching,
case expressions, if expressions) are well suited to describe conditional
assignment in the hardware.

Useful features from the functional perspective, like polymorphism and
higher-order functions and expressions also prove suitable to describe
hardware and our implementation shows that they can be translated to
\VHDL\ as well.

A prototype compiler was created in this research. For this prototype the
Haskell language was chosen as the input language, instead of creating a new
language from scratch. This has enabled creating the prototype rapidly,
allowing for experimenting with various functional language features and
interpretations in Cλash. Even supporting more complex features like
polymorphism and higher-order values has been possible. If a new language and
compiler would have been designed from scratch, that new language would not
have been nearly as advanced as the current version of Cλash.

However, Haskell might not have been the best choice for describing
hardware. Some of the expressiveness it offers is not appropriate for
hardware description (such as infinite recursion or recursive types),
but also some extra syntax sugar could be useful (to reduce
boilerplate). 

The lack of real dependent typing support in Haskell has been a burden.
Haskell provides the tools to create some type level programming
constructs (and is improving fast), but another language might have
support for more advanced dependent types (and even type level operations) as
a fundamental part of the language. The need for dependent typing is
particularly present in Cλash to be able to fix some properties (list length,
recursion depth, etc.) at compile time. Having better support for dependent
typing might allow the use of type-safe recursion in Cλash, though this is
fundamentally still a hard problem.

The choice of describing state very explicitly as extra arguments and
results is a mixed blessing. It provides very explicit specification of
state, which allows for very clear descriptions. This also allows for
easy modification of the description in our normalization program, since
state can be handled just like other arguments and results. 

On the other hand, the explicitness of the states and in particular
substates, mean that more complex descriptions can become cumbersome
very quickly. One finds that dealing with unpacking, passing, receiving
and repacking becomes tedious and even error-prone. Removing some of this
boilerplate would make the language even easier to use.

On the whole, the usefulness of Cλash for describing hardware is not
completely clear yet. Most elements of the language have proven
suitable, and even a real world hardware circuit (a reducer circuit
\cite[baaij09]) has been implemented. However, the language has not been
used during a complete design process, where its rapid prototyping and
reusability qualities could become real advantages, or perhaps the state
boilerplate or synchronicity limitations could become real problems.

It is expected that Cλash will be used as a tool in education at the
University of Twente soon. Hopefully this will provide a better insight
in how the system performs.

The prototype compiler has a clear design. Its front-end is taken from the \GHC\
compiler and desugars Haskell into a small, but functional and typed
language, called \emph{Core}. Cλash adds a transformation system that reduces
this small language to a normal form and a simple back-end that performs a
direct translation to \VHDL. This approach has worked well and should probably
be preserved. Especially the transformation based normalization system is
suitable. It is easy to implement a transformation in the prototype, though it
is not trivial to maintain enough overview to guarantee that the system is
correct and complete. In fact, the current set of transformations is probably
not complete yet, in particular when stateful descriptions are involved.
However, the system can be (and has been) described in a mathematical sense,
allowing us to reason about it and probably also prove various correctness
properties in the future.

The scope of this research has been limited to structural descriptions that
are synchronous in a single clock domain using cycle accurate designs. Even
though this is a broad spectrum already, it may turn out that this scope is
too narrow for practical use of Cλash. A common response from people that hear
about using a functional language for hardware description is that they hope
to be able to provide a concise mathematical description of their algorithm
and have the hardware generated for them. Since this is obviously a different
problem altogether, we could not have hoped to even start solving it. However,
hopefully the current Cλash system provides a solid base on top of which
further experimentation with functional descriptions can be built.
